## ztš

**ALERT:** Moved to [GitLab](https://gitlab.com/Kyarei/zt-).

> Modern Rymakonian for Zašta, the Luminary of [name unkown]. He was not
> the only one, but he was the first.

> Stop trying to be the next iso
>
> ~ Mareck

A sound change applier with the eventual goal of being able to express the
[sound changes](https://github.com/bluebear94/uruwi-conlangs/raw/master/out/7_1_1.pdf)
from Middle Rymakonian to Modern Rymakonian.

### Building

This project uses CMake. Type `cmake .` then `make` if you're on a
non-retarded operating system.

(Protip: `cmake . -DCMAKE_BUILD_TYPE=Debug` builds a debug build and
`cmake . -DCMAKE_BUILD_TYPE=Release` builds a release build.)

You need Boost (for `filesystem`) and the Lua 5.3 libraries installed as well.

`make atest` runs the tests. If you're hacking on ztš, then:

* make sure to run the tests whenever you change the code
* make sure you add tests for new features

To build the manual (requires XeLaTeX and latexmk), use `make manual`.
