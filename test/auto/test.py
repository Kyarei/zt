#!/usr/bin/env python3

import difflib
from pathlib import Path
import subprocess
import sys
import time

def processLine(line, testtree):
  stripped = line.strip()
  replaced = stripped.replace("TESTTREE", testtree)
  return replaced + "\n"

def processLines(s, testtree):
  return [processLine(l, testtree) for l in s.splitlines()]

execPath = sys.argv[1]
testtree = sys.argv[2]
testDir = Path(testtree)
casesDir = testDir / "auto/cases"
outputDir = testDir / "auto/output"

outputDir.mkdir(parents=True, exist_ok=True)

allCases = sorted(casesDir.glob("*.zt"))

nPass = 0
nFail = 0

def passTest(caseName, elapsed=None):
  global nPass
  print(end="Test {:<30} passed".format(caseName), file=sys.stderr)
  if elapsed is None: print("", file=sys.stderr)
  else: print(" with {:>12} ns".format(int(1e9 * elapsed)), file=sys.stderr)
  nPass += 1
def failTest(caseName):
  global nFail
  print("Test {:<30} failed".format(caseName), file=sys.stderr)
  nFail += 1

for ztPath in allCases:
  try:
    caseName = ztPath.stem
    expout = casesDir / ("expected-" + caseName + ".txt")
    output = outputDir / ("actual-" + caseName + ".txt")
    diffpath = outputDir / (caseName + ".diff")
    start = time.perf_counter()
    p = subprocess.run([execPath, str(ztPath)],
      input='',
      stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
      encoding="utf8", timeout=1)
    actualStr = p.stdout
    end = time.perf_counter()
    elapsed = end - start
    statcode = p.returncode
    if -128 <= statcode < -1:
      sys.stderr.write(f"Test crashed and exited with code {statcode}\n")
      failTest(caseName)
    with output.open("w") as fh:
      fh.write(actualStr)
    with expout.open("r") as fh:
      expectedStr = fh.read(None)
      expectedLines = processLines(expectedStr, testtree)
      actualLines = processLines(actualStr, testtree)
      if expectedLines != actualLines:
        # fail
        failTest(caseName)
        diff = difflib.unified_diff(
          expectedLines, actualLines,
          fromfile=str(expout), tofile=str(output))
        sdiff = ''.join(diff)
        sys.stderr.write(sdiff)
        with diffpath.open("w") as fh:
          fh.write(sdiff)
      else:
        # pass
        passTest(caseName, elapsed)
  except TimeoutError as e:
    failTest(caseName)
    print("Timed out; output was:", file=sys.stderr)
    print(e.output())

print("{} passed, {} failed".format(nPass, nFail), file=sys.stderr)

if nFail != 0: sys.exit(1)
