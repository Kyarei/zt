#include "SCA.h"

#include "assert.h"

#include <algorithm>
#include <iostream>

#include "sca_lua.h"
#include "utf8.h"

namespace sca {
  template<typename T>
  void splitIntoPhonemes(
      const SCA& sca, const std::string_view s,
      T& phonemes) {
    size_t ei = s.length();
    if (ei == 0) return;
    const PhonemeSpec* ps = nullptr;
    // Go from the whole string and chop off the last character until
    // we get a match
    std::string st(s);
    while (ei >= 1) {
      Error res = sca.getPhonemeByName(st, ps);
      if (res == ErrorCode::ok) break;
      st.pop_back();
      --ei;
    }
    if (ps != nullptr) {
      phonemes.push_back(std::move(st));
      splitIntoPhonemes(sca, s.substr(ei), phonemes);
    } else {
      // No match found; just take the first codepoint
      UTF8Iterator<const std::string_view> it(s);
      ++it;
      size_t pos = it.position();
      phonemes.push_back(std::string(s.substr(0, pos)));
      splitIntoPhonemes(sca, s.substr(pos), phonemes);
    }
  }
  void splitIntoPhonemes(
      const SCA& sca, const std::string_view s,
      std::vector<MChar>& phonemes) {
    splitIntoPhonemes<std::vector<MChar>>(sca, s, phonemes);
  }
  void splitIntoPhonemes(
      const SCA& sca, const std::string_view s,
      std::deque<std::string>& phonemes) {
    splitIntoPhonemes<std::deque<std::string>>(sca, s, phonemes);
  }
  std::string SCA::wStringToString(const WString& ws) const {
    std::string s;
    for (const auto& wc : ws) {
      auto [matchStart, matchEnd] = getPhonemesBySpec(*wc);
      auto it = phonemes.find(wc->name);
      if (matchStart == matchEnd && it != phonemes.end()) {
        s += "[phoneme/";
        if (wc->charClass == INVALID) s += '*';
        else s += charClasses[wc->charClass].name;
        s += ':';
        bool first = true;
        for (size_t i = 0; i < features.size(); ++i) {
          if (!features[i].isCore) continue;
          if (!first) s += ',';
          size_t k = wc->getFeatureValue(i, *this);
          s += features[i].featureName;
          s += '=';
          s += features[i].instanceNames[k];
          first = false;
        }
        s += "]";
      } else {
        assert(!wc->name.empty());
        s += wc->name;
      }
    }
    return s;
  }
  // I hope features with lots of instances aren't that common.
  Error Feature::getFeatureInstanceByName(
      const std::string& name, size_t& id) const {
    auto it = std::find(instanceNames.begin(), instanceNames.end(), name);
    if (it == instanceNames.end())
      return ErrorCode::noSuchFeatureInstance % name;
    id = it - instanceNames.begin();
    return ErrorCode::ok;
  }
  std::variant<bool, Error>
  SoundChange::apply(
      const SCA& sca, WString& st,
      const std::string& pos,
      const Config& c) const {
    bool matched = false;
    size_t nMatches = 0, maxMatches = c.maxSubsts;
    if (!poses.empty() && poses.count(pos) == 0) return false;
    size_t i = 0;
    // `<=` is intentional. We allow matching one character past the end
    // to allow epenthesis rules such as the following:
    // -> i (t _ ~);
    while (i <= st.size()) {
      if (nMatches >= maxMatches) {
        std::string fname = rule->fname.has_value() ? *(rule->fname) : "";
        return Error(ErrorCode::matchLimitExceeded).at(
          std::move(fname), rule->line, rule->col);
      }
      auto res = (opt.eo == EvaluationOrder::ltr) ?
        rule->tryReplaceLTR(sca, st, i) :
        rule->tryReplaceRTL(sca, st, i);
      CHECK_ERROR_IN_VARIANT(res)
      bool present = std::holds_alternative<size_t>(res);
      if (present) matched = true;
      if (present && opt.beh == Behaviour::once) break;
      if (present && opt.beh == Behaviour::loopnsi)
        i += std::get<size_t>(res);
      else ++i;
      ++nMatches;
    }
    return matched;
  }
  SCA::SCA(sca::lua::InitOptions opts) :
      phonemesReverse(16, PSHash{this}, PSEqual{this}),
      luaState(luaL_newstate(), &lua_close) {
    sca::lua::init(luaState.get(), opts);
  }
  Error SCA::insertFeature(
      Feature&& f, const PhonemesByFeature& phonemesByFeature) {
    size_t oldFeatureCount = features.size();
    auto [elem, succeeded] = featuresByName.insert(
      std::pair{f.featureName, oldFeatureCount});
    if (!succeeded) {
      const Feature& old = features[elem->second];
      return (ErrorCode::featureExists % f.featureName).at(old.line, old.col);
    }
    features.push_back(std::move(f));
    for (size_t ii = 0; ii < phonemesByFeature.size(); ++ii) {
      const std::vector<std::string>& phonemesInInstance =
        phonemesByFeature[ii];
      for (const std::string& phoneme : phonemesInInstance) {
        PhonemeSpec& spec = phonemes[phoneme];
        spec.name = phoneme;
        spec.setFeatureValue(oldFeatureCount, ii, *this);
      }
    }
    return ErrorCode::ok;
  }
  Error SCA::insertClass(
      std::string&& name, const std::vector<std::string>& myPhonemes,
      size_t line, size_t col) {
    size_t oldClassCount = charClasses.size();
    auto [elem, succeeded] = classesByName.insert(
      std::pair{name, oldClassCount});
    if (!succeeded) {
      const CharClass& old = charClasses[elem->second];
      return (ErrorCode::classExists % name).at(old.line, old.col);
    }
    for (const std::string& phoneme : myPhonemes) {
      PhonemeSpec& spec = phonemes[phoneme];
      if (spec.charClass != INVALID)
        return ErrorCode::phonemeAlreadyHasClass %
          (phoneme + " is in " + charClasses[spec.charClass].name +
            "; tried to insert it in " + name);
      if (spec.name.empty()) spec.name = phoneme;
    }
    charClasses.emplace_back();
    CharClass& newClass = charClasses.back();
    newClass.name = std::move(name);
    newClass.line = line;
    newClass.col = col;
    for (const std::string& phoneme : myPhonemes) {
      PhonemeSpec& spec = phonemes[phoneme];
      spec.charClass = oldClassCount;
    }
    return ErrorCode::ok;
  }
  Error SCA::getFeatureByName(
      const std::string& name, size_t& id, Feature const*& feature) const {
    auto it = featuresByName.find(name);
    if (it == featuresByName.end())
      return ErrorCode::noSuchFeature % name;
    id = it->second;
    feature = &(features[it->second]);
    return ErrorCode::ok;
  }
  Error SCA::getClassByName(
      const std::string& name, size_t& id, CharClass const*& cclass) const {
    auto it = classesByName.find(name);
    if (it == classesByName.end())
      return ErrorCode::noSuchClass % name;
    id = it->second;
    cclass = &(charClasses[it->second]);
    return ErrorCode::ok;
  }
  Error SCA::getSyllabifierByName(
      const std::string& name, size_t& id, Syllabifier const*& syll) const {
    auto it = syllabifiersByName.find(name);
    if (it == syllabifiersByName.end())
      return ErrorCode::noSuchClass % name;
    id = it->second;
    syll = &(syllabifiers[it->second]);
    return ErrorCode::ok;
  }
  Error SCA::getPhonemeByName(
      const std::string& name, PhonemeSpec const*& ps) const {
    auto it = phonemes.find(name);
    if (it == phonemes.end())
      return ErrorCode::noSuchPhoneme % name;
    ps = &(it->second);
    return ErrorCode::ok;
  }
  Error SCA::getFeatureByName(
      const std::string& name, size_t& id, Feature*& feature) {
    return getFeatureByName(name, id, (const Feature*&) feature);
  }
  Error SCA::getClassByName(
      const std::string& name, size_t& id, CharClass*& cclass) {
    return getClassByName(name, id, (const CharClass*&) cclass);
  }
  Error SCA::getSyllabifierByName(
      const std::string& name, size_t& id, Syllabifier*& syll) {
    return getSyllabifierByName(name, id, (const Syllabifier*&) syll);
  }
  Error SCA::getPhonemeByName(
      const std::string& name, PhonemeSpec*& ps) {
    return getPhonemeByName(name, (const PhonemeSpec*&) ps);
  }
  void SCA::verify(std::vector<Error>& errors) const {
    for (const SoundChange& sc : rules) {
      sc.rule->verify(errors, *this, sc);
    }
  }
  void SCA::reversePhonemeMap() {
    for (const auto& [name, spec] : phonemes) {
      phonemesReverse.insert(std::pair(spec, name));
    }
  }
  std::variant<std::string, Error> SCA::apply(
      const std::string_view& st,
      const std::string& pos,
      const Config& c) const {
    bool verbose = c.verbose;
    // Split into phonemes
    MString ms;
    splitIntoPhonemes(*this, st, ms);
    // Map to actual PhonemeSpec objects
    WString ws;
    for (const MChar& ch : ms) {
      const PhonemeSpec* ps;
      assert(ch.is<std::string>());
      auto res = getPhonemeByName(ch.as<std::string>(), ps);
      if (res.ok()) {
        ws.push_back(makePObserver(*ps));
      } else {
        // None found; create a temporary
        // (would have liked to cache this but this method is const)
        auto ps2 = makePOwner<PhonemeSpec>();
        ps2->name = std::move(ch.as<std::string>());
        ws.push_back(makeConst(std::move(ps2)));
      }
    }
    // std::cerr << wStringToString(ws) << "\n";
    std::string s;
    for (const SoundChange& r : rules) {
      if (verbose) {
        s = wStringToString(ws);
      }
      auto res = r.apply(*this, ws, pos, c);
      CHECK_ERROR_IN_VARIANT(res)
      bool matched = std::get<bool>(res);
      if (verbose && matched) {
        std::cerr << s << " -> " << wStringToString(ws);
        std::cerr << " (";
        if (r.rule->fname.has_value())
          std::cerr << *(r.rule->fname) << ":";
        std::cerr << (r.rule->line + 1) << ":";
        std::cerr << (r.rule->col + 1) << ")";
        std::cerr << "\n";
      }
      // std::cerr << "-> " << wStringToString(ws) << "\n";
    }
    return wStringToString(ws);
  }
  void SCA::addGlobalLuaCode(const LuaCode& lc) {
    globalLuaCode += lc.code;
  }
  std::string SCA::executeGlobalLuaCode() {
    sca::lua::pushSCAConst(luaState.get(), *this);
    lua_setglobal(luaState.get(), "sca");
    if (globalLuaCode.empty()) return "";
    int stat = luaL_loadbufferx(
      luaState.get(),
      globalLuaCode.c_str(), globalLuaCode.size(),
      "<global code>", "t");
    if (stat != LUA_OK) goto rek;
    stat = lua::mycall(luaState.get(), 0, 0);
    if (stat != LUA_OK) goto rek;
    return "";
    rek:
    std::string s = lua_tostring(luaState.get(), -1);
    lua_pop(luaState.get(), -1);
    return s;
  }
  std::string SCA::checkGlobalLuaCode() {
    sca::lua::pushSCAConst(luaState.get(), *this);
    lua_setglobal(luaState.get(), "sca");
    if (globalLuaCode.empty()) return "";
    int stat = luaL_loadbufferx(
      luaState.get(),
      globalLuaCode.c_str(), globalLuaCode.size(),
      "<global code>", "t");
    if (stat != LUA_OK) goto rek;
    // Nothing is executed
    return "";
    rek:
    std::string s = lua_tostring(luaState.get(), -1);
    lua_pop(luaState.get(), -1);
    return s;
  }
}
