#include "config.h"

#include <stdio.h>
#include <string.h>

namespace sca {
  const char* defaultFormat = "%A%?p[#]%P -> %O";
  static const char* usage = R".(Usage:
  %s [options...] <script.zt> [words.txt]

  * <script.zt>: a path to a ztš script to apply to the words
  * <words.txt>: a path to a file of newline-separated words, or stdin
      if omitted. If a '#' is found on a line, the substring after it
      will be passed as the part of speech, while the actual word is
      truncated before the '#'.
  * -v, --verbose: verbose output (invocations output to stderr)
  * -f, --format <formatter=%%A%%?p[#]%%P -> %%O>: a format string for the output:
    * %%%%: a literal '%%' sign
    * %%a: the input word, without the part of speech
    * %%o: the output word
    * %%p: the part of speech
    * %%?*[...]: prints the string inside the square brackets if a predicate
      is true (given by whatever * is):
      * p: part of speech present
      * P: part of speech absent
  * -e, --escape <chars=\>: list characters to escape when outputting from %%a,
    %%o or %%p in the formatter. These specifiers then precede any instances
    of those characters. This is intended to produce output that other
    programs can parse unambiguously; it does not handle Unicode properly.
  * -m, --max-substs <num>: specify the maximum number of substitutions to make
    for each rule (default: 5000)
  * -S<var>=<value>: define <var> to be the string value <value> in the
    preprocessor
  * -N<var>=<value>: define <var> to be the numeric value <value> in the
    preprocessor
  * -I<path>: add <path> to the list of included paths
  * -j<n>: use <n> work threads (defaults to %zu).
  * --syntax-only: stop after checking syntax.
  * --lua-enable-io, --lua-enable-os, --lua-enable-debug: load the respective
    Lua libraries, which are omitted by default due to security concerns.
  
See the manual for documentation on the ztš language.
  ).";

  static void printUsage(const char* programName) {
    fprintf(stderr, usage, programName, nWorkThreads);
  }
  void parse(Config& c, int argc, char** argv) {
    char** w = argv + 1;
    unsigned pos = 0;
    bool stopOptions = false;
    while (*w != nullptr) {
      unsigned mode = 0;
      sca::lua::InitOptions toAdd = sca::lua::InitOptions::none;
      char* arg = *(w++);
      if (arg[0] == '-' && !stopOptions) {
        switch (arg[1]) {
          case '-': {
            if (arg[2] == '\0') mode = 5;
            else if (strcmp(arg + 2, "format") == 0) mode = 1;
            else if (strcmp(arg + 2, "escape") == 0) mode = 2;
            else if (strcmp(arg + 2, "verbose") == 0) mode = 3;
            else if (strcmp(arg + 2, "syntax-only") == 0) mode = 4;
            else if (strcmp(arg + 2, "max-substs") == 0) mode = 7;
            else if (strcmp(arg + 2, "lua-enable-io") == 0) {
              toAdd = sca::lua::InitOptions::io;
              mode = 6;
            } else if (strcmp(arg + 2, "lua-enable-os") == 0) {
              toAdd = sca::lua::InitOptions::os;
              mode = 6;
            } else if (strcmp(arg + 2, "lua-enable-debug") == 0) {
              toAdd = sca::lua::InitOptions::debug;
              mode = 6;
            }
            else mode = -1;
            break;
          }
          case 'f': mode = 1; break;
          case 'e': mode = 2; break;
          case 'v': mode = 3; break;
          case 'm': mode = 7; break;
          case 'I': c.pathList.push_back(arg + 2); break;
          case 'S': {
            char* eloc = strchr(arg + 2, '=');
            if (eloc == nullptr) {
              mode = -1;
              break;
            }
            std::string key(arg + 2, eloc - (arg + 2));
            std::string val(eloc + 1);
            c.stringValues.emplace_back(std::move(key), std::move(val));
            mode = -2;
            break;
          }
          case 'N': {
            char* eloc = strchr(arg + 2, '=');
            if (eloc == nullptr) {
              mode = -1;
              break;
            }
            std::string key(arg + 2, eloc - (arg + 2));
            char* endptr;
            size_t val = (size_t) strtoull(eloc + 1, &endptr, 10);
            if (endptr[0] != '\0') {
              mode = -1;
              break;
            }
            c.numValues.emplace_back(std::move(key), val);
            mode = -2;
            break;
          }
          case 'j': {
            char* endptr;
            size_t val = (size_t) strtoull(arg + 2, &endptr, 10);
            if (endptr[0] != '\0') {
              mode = -1;
              break;
            }
            c.nThreads = val;
            mode = -2;
            break;
          }
          default: mode = -1; break;
        }
      }
      if (mode == 1) {
        char* fmter = *(w++);
        if (fmter == nullptr) mode = -1;
        else c.format = fmter;
      } else if (mode == 2) {
        char* escapes = *(w++);
        if (escapes == nullptr) mode = -1;
        else c.escapes = escapes;
      } else if (mode == 3) {
        c.verbose = true;
      } else if (mode == 4) {
        c.syntaxOnly = true;
      } else if (mode == 5) {
        stopOptions = true;
      } else if (mode == 6) {
        c.opts = c.opts | toAdd;
      } else if (mode == 7) {
        char* num = *(w++);
        if (num == nullptr) mode = -1;
        char* endptr;
        size_t val = (size_t) strtoull(num, &endptr, 10);
        if (endptr[0] != '\0') {
          mode = -1;
          break;
        }
        c.maxSubsts = val;
      } else if (mode == 0) {
        switch (pos++) {
          case 0: c.script = arg; break;
          case 1: c.words = arg; break;
          default: mode = -1;
        }
      }
      if (mode == (unsigned) -1) {
        printUsage(argv[0]);
        exit(1);
      }
    }
    if (pos < 1) {
      printUsage(argv[0]);
      exit(1);
    }
  }
}
