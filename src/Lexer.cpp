#include "Lexer.h"

#include <ctype.h>

#include <algorithm>
#include <iostream>

#include <boost/filesystem.hpp>

#include <lua.hpp>

#include "sca_lua.h"
#include "utf8.h"

using namespace sca::directive;
namespace fs = boost::filesystem;

namespace sca {
  static Error& at(Error& e, const Cursor& c) {
    return c.fname.has_value() ?
      e.at(std::string(*c.fname), c.line, c.col) :
      e.at(c.line, c.col);
  }
  int Cursor::read() noexcept {
    s->clear();
    s->seekg(off);
    int c = s->get();
    if (c == '\n') {
      ++line;
      col = 0;
    } else if (c != std::char_traits<char>::eof()) {
      int d = s->peek();
      // Crude way to determine start of next UTF-8 codepoint --
      // will probably break if source file is not actually UTF-8
      if (d == std::char_traits<char>::eof() || !isContinuation((char) d))
        ++col;
    }
    off = s->tellg();
    return c;
  }
  int Cursor::peek() noexcept {
    s->clear();
    s->seekg(off);
    return s->peek();
  }
  Token empty(const Cursor& c) noexcept {
    Token t;
    t.line = c.line;
    t.col = c.col;
    t.fname = c.fname;
    return t;
  }
  inline bool isStringChar(int c) {
    return isalpha(c) || (unsigned char) c >= 0x80;
  }
  Lexer::Lexer(
    std::shared_ptr<std::istream>&& in,
    const std::optional<std::string>& fname) :
      luaState(luaL_newstate(), &lua_close) {
    if (fname.has_value()) {
      cursors.emplace_back(std::move(in), *fname);
      searchPaths.push_back(fs::absolute(*fname).parent_path());
    } else {
      cursors.emplace_back(std::move(in));
    }
  }
  void Lexer::setStringVar(const char* key, const std::string& val) {
    lua_State* l = luaState.get();
    lua_pushlstring(l, val.c_str(), val.size());
    lua_setglobal(l, key);
  }
  void Lexer::setNumVar(const char* key, size_t val) {
    lua_State* l = luaState.get();
    lua_pushinteger(l, val);
    lua_setglobal(l, key);
  }
  void Lexer::discardWhitespace() noexcept {
    int c;
    while (true) {
      c = cursor().peek();
      if (c == std::char_traits<char>::eof() || !isspace(c)) break;
      cursor().read();
    }
  }
  std::string Lexer::getRestOfLine() noexcept {
    std::string s;
    while (true) {
      int c = cursor().read();
      if (c == std::char_traits<char>::eof() || c == '\n') break;
      s += (char) c;
    }
    return s;
  }
  std::string Lexer::getRestOfLineStrip() noexcept {
    std::string s = getRestOfLine();
    auto it = std::find_if_not(s.begin(), s.end(), isspace);
    auto it2 = std::find_if_not(s.rbegin(), s.rend(), isspace).base();
    return std::string(it, it2);
  }
  std::variant<Token, Directive, Error> Lexer::getNextTD() noexcept {
    #define RETURN_OP(x) \
      do { \
        t.contents = x; \
        return t; \
      } while (0)
    constexpr auto eof = std::char_traits<char>::eof();
    int c;
    do {
      c = cursor().read();
      while (c == '#') {
        // Comment; read the first character after the newline
        do {
          c = cursor().read();
        } while (c != '\n' && c != eof);
        c = cursor().read();
      }
    } while (isspace(c));
    Token t = empty(cursor());
    switch (c) {
      case eof: {
        return t; // Already EOF
      }
      case '(': RETURN_OP(Operator::lb);
      case ')': RETURN_OP(Operator::rb);
      case '[': RETURN_OP(Operator::lsb);
      case ']': RETURN_OP(Operator::rsb);
      case '{': RETURN_OP(Operator::lcb);
      case '}': RETURN_OP(Operator::rcb);
      case ',': RETURN_OP(Operator::comma);
      case '=': RETURN_OP(Operator::equals);
      case ':': RETURN_OP(Operator::colon);
      case ';': RETURN_OP(Operator::semicolon);
      case '_': RETURN_OP(Operator::placeholder);
      case '~': RETURN_OP(Operator::boundary);
      case '/': RETURN_OP(Operator::slash);
      case '*': RETURN_OP(Operator::star);
      case '+': RETURN_OP(Operator::plus);
      case '?': RETURN_OP(Operator::question);
      case '.': RETURN_OP(Operator::dot);
      case '<': {
        Cursor temp = cursor();
        int d = cursor().read();
        if (d == '=') RETURN_OP(Operator::le);
        cursor() = std::move(temp);
        RETURN_OP(Operator::lt);
      }
      case '>': {
        Cursor temp = cursor();
        int d = cursor().read();
        if (d == '=') RETURN_OP(Operator::ge);
        cursor() = std::move(temp);
        RETURN_OP(Operator::gt);
      }
      case '|': {
        Cursor temp = cursor();
        int d = cursor().read();
        if (d == '|') {
          RETURN_OP(Operator::envOr);
        }
        cursor() = std::move(temp);
        RETURN_OP(Operator::pipe);
      }
      case '!': {
        Cursor temp = cursor();
        int d = cursor().read();
        if (d == '=') {
          RETURN_OP(Operator::notEquals);
        }
        cursor() = std::move(temp);
        RETURN_OP(Operator::bang);
      }
      case '$': {
        Cursor temp = cursor();
        int d = cursor().read();
        switch (d) {
          case '(': RETURN_OP(Operator::dlb);
          case '$': {
            // Get characters until a `$$`
            std::string code;
            while (true) {
              int d = cursor().read();
              if (d == eof)
                return ErrorCode::lexerGeneric % "expected $, got EOF";
              if (d == '$' && code.back() == '$') break;
              code += (char) d;
            }
            code.pop_back();
            t.contents = LuaCode{std::move(code)};
            return std::move(t);
          }
          default: {
            cursor() = std::move(temp);
            return ErrorCode::lexerGeneric %
              "expected $ or (, got something else";
          }
        }
      }
      case '-': {
        Cursor temp = cursor();
        int d = cursor().read();
        switch (d) {
          case '>': RETURN_OP(Operator::arrow);
          default: {
            cursor() = std::move(temp);
            return ErrorCode::lexerGeneric %
              "expected -, got something else";
          }
        }
      }
      case 0x22: {
        std::string s;
        while (true) {
          int d = cursor().read();
          if (d == eof)
            return ErrorCode::lexerGeneric % "expected \", got EOF";
          if (d == 0x22) {
            t.contents = std::move(s);
            return t;
          } else if (d == '\\') {
            int e = cursor().read();
            switch (e) {
              case eof:
                return ErrorCode::lexerGeneric % "expected \", got EOF";
              case 0x22: s += (char) 0x22; break;
              case '\\': s += '\\'; break;
              case 'n': s += '\n'; break;
            }
          } else {
            s += (char) d;
          }
        }
      }
      case '%': {
        #define FETCH_TOKEN(name) \
          auto name##TD = getNextTD(); \
          if (std::holds_alternative<Error>(name##TD)) \
            return name##TD; \
          else if (std::holds_alternative<Directive>(name##TD)) \
            return ErrorCode::lexerGeneric % "expected token, got directive"; \
          auto name = std::get<Token>(name##TD);
        #define TO_STRING(tname, sname) \
          if (!tname.is<std::string>()) \
            return ErrorCode::lexerGeneric % "expected string, got something else"; \
          std::string sname = tname.as<std::string>();
        #define CHECK_ALNUM(x) \
          if (!std::all_of(x.begin(), x.end(), isalnum)) { \
            return Error(ErrorCode::ifdefSpace); \
          }
        // Process directive
        std::string name;
        while (true) {
          int d = cursor().read();
          if (d == eof || isspace(d)) break;
          name += (char) d;
        }
        discardWhitespace();
        auto l = luaState.get();
        if (name == "if") {
          std::string exs = getRestOfLineStrip();
          int expr = lua::createExpression(l, exs);
          if (expr == LUA_NOREF)
            return ErrorCode::luaSyntax % lua_tostring(l, -1);
          return If{expr, false};
        } else if (name == "ifdef") {
          FETCH_TOKEN(t) TO_STRING(t, exs)
          CHECK_ALNUM(exs)
          return IfDef{exs, false};
        } else if (name == "else") {
          return Else{};
        } else if (name == "else_if") {
          std::string exs = getRestOfLineStrip();
          int expr = lua::createExpression(l, exs);
          if (expr == LUA_NOREF)
            return ErrorCode::luaSyntax % lua_tostring(l, -1);
          return If{expr, true};
        } else if (name == "else_ifdef") {
          FETCH_TOKEN(t) TO_STRING(t, exs)
          CHECK_ALNUM(exs)
          return IfDef{exs, true};
        } else if (name == "end") {
          return End{};
        } else if (name == "define") {
          FETCH_TOKEN(t) TO_STRING(t, name)
          CHECK_ALNUM(name)
          FETCH_TOKEN(t2)
          return std::visit([&](auto&& arg) -> decltype(getNextTD()) {
            using T = std::decay_t<decltype(arg)>;
            if constexpr (std::is_same_v<T, std::string>
                       || std::is_same_v<T, size_t>)
              return Define{name, arg};
            return ErrorCode::lexerGeneric %
              "expected string or number, got something else";
          }, t2.contents);
        } else if (name == "undef") {
          FETCH_TOKEN(t) TO_STRING(t, name)
          CHECK_ALNUM(name)
          return Undef{name};
        } else if (name == "include") {
          FETCH_TOKEN(t) TO_STRING(t, name)
          return Include{name, false, false};
        } else if (name == "include_lua") {
          FETCH_TOKEN(t) TO_STRING(t, name)
          return Include{name, true, false};
        } else if (name == "include_once") {
          FETCH_TOKEN(t) TO_STRING(t, name)
          return Include{name, false, true};
        } else if (name == "include_lua_once") {
          FETCH_TOKEN(t) TO_STRING(t, name)
          return Include{name, true, true};
        } else if (name == "export") {
          FETCH_TOKEN(t) TO_STRING(t, name)
          CHECK_ALNUM(name)
          return Export{name};
        }
        (void) getRestOfLine();
        return ErrorCode::unrecognisedDirective % name;
      }
      default: {
        if (isStringChar(c)) {
          // Alphabetic: treat this as a string
          std::string s{(char) c};
          int d;
          while (true) {
            d = cursor().peek();
            if (isStringChar(d) && d != std::char_traits<char>::eof()) {
              s += (char) d;
              (void) cursor().read();
            } else break;
          }
          if (s == "feature") t.contents = Operator::kwFeature;
          else if (s == "class") t.contents = Operator::kwClass;
          else if (s == "NOT") t.contents = Operator::bang;
          else if (s == "ordered") t.contents = Operator::kwOrdered;
          else if (s == "executeOnce") t.contents = Operator::kwExecuteOnce;
          else if (s == "setOptions") t.contents = Operator::kwSetOptions;
          else if (s == "EOF") t.contents = EndOfFile{};
          else t.contents = std::move(s);
          return t;
        } else if (isdigit(c)) {
          size_t n = c - '0';
          int d;
          while (true) {
            d = cursor().peek();
            if (isdigit(d)) {
              n *= 10;
              n += c - '0';
              (void) cursor().read();
            } else break;
          }
          t.contents = n;
          return t;
        }
      }
    }
    return ErrorCode::lexerGeneric %
      (std::string("Unrecognised character ") + (char) c);
  }
  std::optional<Token> Lexer::getNext() noexcept {
    if (retained.has_value()) {
      Token t; t.contents = std::move(*retained);
      retained = std::nullopt;
      return t;
    }
    auto t = getNextTD();
    return std::visit([this](auto&& arg) -> std::optional<Token> {
      using T = std::decay_t<decltype(arg)>;
      if constexpr (std::is_same_v<T, Token>) {
        if (arg.template is<EndOfFile>()) {
          if (cursors.size() <= 1) return std::move(arg);
          cursors.pop_back();
          return getNext();
        }
        return isValid() ? std::move(arg) : getNext();
      } else if constexpr (std::is_same_v<T, Directive>) {
        Error e = handleDirective(arg);
        if (e.ec == ErrorCode::internalIncludeLua) {
          retained = LuaCode{e.details};
          Token t; t.contents = Operator::kwExecuteOnce;
          return t;
        }
        if (e.ok()) return getNext();
        printError(at(e, cursor()));
        return std::nullopt;
      } else if constexpr (std::is_same_v<T, Error>) {
        printError(at(arg, cursor()));
        return std::nullopt;
      }
    }, std::move(t));
  }
  static std::variant<bool, Error> evaluate(const If& d, lua_State* l) {
    lua_geti(l, LUA_REGISTRYINDEX, d.luaexpr);
    int stat = lua::mycall(l, 0, 1);
    if (stat != LUA_OK) {
      return ErrorCode::luaSyntax % lua_tostring(l, -1);
    }
    return lua_toboolean(l, -1);
  }
  static std::variant<bool, Error> evaluate(const IfDef& d, lua_State* l) {
    int type = lua_getglobal(l, d.name.c_str());
    lua_pop(l, 1);
    return type != LUA_TNIL;
  }
  std::variant<std::string, boost::system::error_code>
  Lexer::searchFor(const std::string& rpath) const noexcept {
    using namespace boost::system;
    error_code ec = make_error_code(errc::no_such_file_or_directory);
    for (const auto& p : searchPaths) {
      error_code ectemp;
      auto path = fs::canonical(p / rpath, ectemp);
      if (!ectemp) {
        return path.string();
      }
      if (ectemp != make_error_code(errc::no_such_file_or_directory))
        ec = std::move(ectemp);
    }
    return ec;
  }
  Error Lexer::handleDirective(const Directive& d) noexcept {
    return std::visit([this](auto&& arg) -> Error {
      using T = std::decay_t<decltype(arg)>;
      lua_State* l = luaState.get();
      (void) l; // avoid unused but set variable warnings on GCC
      if constexpr (std::is_same_v<T, If> || std::is_same_v<T, IfDef>) {
        if (arg.isElse) {
          if (ifs.empty()) return ErrorCode::elseWithoutIf;
          // Flip condition if not wrapped in an unevaluated block
          if (ifs.size() == 1 || ifs[ifs.size() - 2].isValid()) {
            auto& back = ifs.back();
            bool oldCurr = back.current;
            if (!back.expired) back.current = !back.current;
            if (oldCurr) back.expired = true;
            auto res = evaluate(arg, l);
            if (std::holds_alternative<Error>(res)) return std::get<Error>(res);
            back.current = std::get<bool>(res);
          }
        }
        // New if statement
        else if (isValid()) {
          auto res = evaluate(arg, l);
          if (std::holds_alternative<Error>(res)) return std::get<Error>(res);
          ifs.push_back({std::get<bool>(res), false});
        } else {
          ifs.push_back({false, false});
        }
      } else if constexpr (std::is_same_v<T, Else>) {
        if (ifs.empty()) return ErrorCode::elseWithoutIf;
        // Flip condition if not wrapped in an unevaluated block
        if (ifs.size() == 1 || ifs[ifs.size() - 2].isValid()) {
          auto& back = ifs.back();
          if (back.current) back.expired = true;
          back.current = !back.current;
        }
      } else if constexpr (std::is_same_v<T, End>) {
        if (ifs.empty())
          return ErrorCode::endWithoutIf;
        ifs.pop_back();
      } else if constexpr (std::is_same_v<T, Define>) {
        if (!isValid()) return ErrorCode::ok;
        if (std::holds_alternative<std::string>(arg.value)) {
          const auto& s = std::get<std::string>(arg.value);
          lua_pushlstring(l, s.c_str(), s.size());
        } else {
          lua_pushinteger(l, std::get<size_t>(arg.value));
        }
        lua_setglobal(l, arg.name.c_str());
      } else if constexpr (std::is_same_v<T, Undef>) {
        if (!isValid()) return ErrorCode::ok;
        lua_pushnil(l);
        lua_setglobal(l, arg.name.c_str());
      } else if constexpr (std::is_same_v<T, Include>) {
        if (!isValid()) return ErrorCode::ok;
        auto p = searchFor(arg.fname);
        if (std::holds_alternative<boost::system::error_code>(p)) {
          return ErrorCode::fileError %
            (std::get<boost::system::error_code>(p).message() +
              " for " + arg.fname);
        }
        std::string path = std::move(std::get<std::string>(p));
        if (arg.once && included.count(path) != 0) return ErrorCode::ok;
        included.insert(path);
        if (arg.isLua) {
          std::ifstream fh(path);
          fh.seekg(0, std::ios_base::end);
          size_t len = fh.tellg();
          fh.seekg(0);
          std::string contents(len, '\0');
          fh.read(contents.data(), len);
          return ErrorCode::internalIncludeLua % contents;
        }
        cursors.emplace_back(std::make_shared<std::ifstream>(path), path);
      } else if constexpr (std::is_same_v<T, Export>) {
        if (!isValid()) return ErrorCode::ok;
        addExport(arg.name);
      }
      return ErrorCode::ok;
    }, d.value);
  }
  void Lexer::exportVariables(lua_State* l2) {
    lua_State* l = luaState.get();
    for (const std::string& name : exports) {
      lua_getglobal(l, name.c_str());
      lua_xmove(l, l2, 1);
      lua_setglobal(l2, name.c_str());
    }
  }
}
