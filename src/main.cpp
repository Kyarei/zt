#include <stdio.h>
#include <atomic>
#include <condition_variable>
#include <fstream>
#include <iostream>
#include <mutex>
#include <optional>
#include <string>
#include <thread>
#include <type_traits>
#include <vector>

#include <boost/filesystem.hpp>

#include "config.h"
#include "Lexer.h"
#include "Parser.h"
#include "Rule.h"
#include "SCA.h"
#include "Token.h"
#include "errors.h"
#include "sca_lua.h"

namespace fs = boost::filesystem;

std::string escape(const std::string_view& s, const char* escapes) {
  std::string res;
  for (char c : s) {
    if (strchr(escapes, c) != nullptr) res += '\\';
    res += c;
  }
  return res;
}

std::string format(
    const char* pattern,
    const std::string_view& a, const std::string_view& o,
    const std::string_view& p,
    const char* escapes) {
  std::string res;
  std::string ae = escape(a, escapes);
  std::string oe = escape(o, escapes);
  std::string pe = escape(p, escapes);
  const char* w = pattern;
  while (*w != '\0') {
    if (*w != '%') {
      res += *w;
    } else switch (char opt = *(++w)) {
      case '%': res += '%'; break;
      case 'a': res += ae; break;
      case 'o': res += oe; break;
      case 'p': res += pe; break;
      case 'A': res += a; break;
      case 'O': res += o; break;
      case 'P': res += p; break;
      case '?': {
        char c = *(++w);
        bool predKnown = true;
        bool pred = false;
        switch (c) {
          case 'p': pred = !p.empty(); break;
          case 'P': pred = p.empty(); break;
          default: predKnown = false;
        }
        if (predKnown && *(++w) == '[') {
          while (*(++w) != ']') {
            if (*w == '\0') {
              std::cerr << "Unclosed conditional " << *w << "\n";
            }
            if (pred) res += *w;
          }
        } else {
          std::cerr << "Unknown condition " << c << "\n";
          exit(-1);
        }
        break;
      }
      case '\0':
      std::cerr << "% at end of formatter\n";
      exit(1);
      default:
      std::cerr << "Unknown option " << opt << "\n";
      exit(1);
    }
    ++w;
  }
  return res;
}

void work(const sca::Config& c, const sca::SCA& mysca, std::istream& wfh) {
  std::atomic_uint readIndex = 0;
  std::atomic_uint writeIndex = 0;
  std::mutex fileLock;
  std::mutex writeLock;
  std::condition_variable writeCV;
  std::vector<std::thread> threads(c.nThreads);
  for (size_t i = 0; i < c.nThreads; ++i) {
    std::thread worker([&]() {
      std::string line;
      while (true) {
        fileLock.lock();
        unsigned index = readIndex++;
        std::getline(wfh, line);
        bool isEOF = wfh.eof();
        fileLock.unlock();
        if (isEOF && line.empty()) break;
        if (line.empty()) {
          ++writeIndex;
          continue;
        }
        size_t i = line.find("#");
        std::string pos;
        if (i != std::string::npos) {
          pos = line.substr(i + 1);
          line.resize(i);
        }
        auto res = mysca.apply(line, pos, c);
        // Wait until it's this thread's turn to write
        {
          std::unique_lock<std::mutex> wl(writeLock);
          while (writeIndex != index) writeCV.wait(wl);
          if (std::holds_alternative<std::string>(res)) {
            const std::string& output = std::get<std::string>(res);
            std::cout
              << format(
                c.format, line, output, pos, c.escapes)
              << "\n";
          } else {
            sca::printError(std::get<sca::Error>(res));
            exit(1);
          }
          ++writeIndex;
          wl.unlock();
          writeCV.notify_all();
        }
        line.clear();
      }
    });
    threads[i] = std::move(worker);
  }
  for (size_t i = 0; i < c.nThreads; ++i) threads[i].join();
}

int main(int argc, char** argv) {
  // Read config
  sca::Config c;
  sca::parse(c, argc, argv);
  if (!fs::exists(c.script) || fs::is_directory(c.script)) {
    std::cerr << "File " << c.script << " doesn't exist or is a directory\n";
    return 1;
  }
  if (c.words != nullptr &&
      (!fs::exists(c.words) || fs::is_directory(c.words))) {
    std::cerr << "File " << c.words << " doesn't exist or is a directory\n";
    return 1;
  }
  // Lex and parse
  auto fh = std::make_shared<std::ifstream>(c.script);
  sca::Lexer lexer(fh, c.script);
  for (auto&& path : c.pathList) lexer.addIncludePath(std::move(path));
  for (const auto& [name, value] : c.stringValues)
    lexer.setStringVar(name.c_str(), value);
  for (const auto& [name, value] : c.numValues)
    lexer.setNumVar(name.c_str(), value);
  sca::SCA mysca(c.opts);
  sca::Parser parser(&lexer, &mysca);
  bool res = parser.parse();
  if (!res) return 1;
  // Verify SCA stuff
  std::vector<sca::Error> errors;
  mysca.verify(errors);
  for (const sca::Error& e : errors)
    sca::printError(e);
  if (!errors.empty()) return 1;
  if (c.syntaxOnly) {
    lexer.exportVariables(mysca.getLuaState());
    std::string err = mysca.checkGlobalLuaCode();
    if (!err.empty()) {
      std::cerr << err << '\n';
      return 1;
    }
    return 0;
  }
  // Pre-execute
  mysca.reversePhonemeMap();
  lexer.exportVariables(mysca.getLuaState());
  std::string err = mysca.executeGlobalLuaCode();
  if (!err.empty()) {
    std::cerr << err << '\n';
    return 1;
  }
  if (c.nThreads != 0 && c.verbose) {
    std::cerr << "Warning: multithreading not supported with verbosity.\n";
    c.nThreads = 1;
  }
  if (c.nThreads == 0) {
    c.nThreads = c.verbose ? 1 : sca::nWorkThreads;
  }
  std::istream* wfh = (c.words != nullptr) ?
    new std::fstream(c.words) : &(std::cin);
  // Apply sound changes for each word in input
  // First, work on the remaining input in the source (if EOFed early)
  if (fh->good()) work(c, mysca, *fh);
  // Then the input file
  work(c, mysca, *wfh);
  if (c.words != nullptr) delete wfh;
  return 0;
}
