#include "sca_lua.h"

#include <assert.h>
#include <string.h>

#include "Rule.h"
#include "SCA.h"
#include "lua/metatable_names.h"
#include "lua/macro_magic.h"

namespace sca::lua {
  // ======================== PS management =========================
  constexpr size_t ELEMS_PER_BLOCK = 16;
  static constexpr const char*
    ctxName = "zt_scapscontext";
  static int destroySCAPSContext(lua_State* l) {
    SCAPSContext* ctx = (SCAPSContext*) lua_touserdata(l, 1);
    ctx->~SCAPSContext();
    return 0;
  }
  static const PhonemeSpec& lookup(
      SCAPSContext& ctx, const SCA& sca, const PhonemeSpec& ps) {
    assert(ctx.canCreate);
    auto [matchStart, matchEnd] = sca.getPhonemesBySpec(ps);
    if (matchStart == matchEnd) {
      // Insert temporary
      return *ctx.create(PhonemeSpec(ps));
    } else {
      auto it = std::find_if(matchStart, matchEnd, [&](const auto& p) {
        return p.first.name == ps.name;
      });
      if (it == matchEnd)
        return matchStart->first;
      else
        return it->first;
    }
  }
  void SCAPSContext::appendElem() {
    created.emplace_back();
  }
  PhonemeSpec* SCAPSContext::create(PhonemeSpec&& ps) {
    if (created.empty() || created.back().elems == ELEMS_PER_BLOCK)
      appendElem();
    Entry& e = created.back();
    e.data[e.elems] = std::move(ps);
    return &(e.data[e.elems++]);
  }
  bool SCAPSContext::contains(const PhonemeSpec* ps) const {
    for (const Entry& e : created) {
      if (ps >= e.data.get() && ps < e.data.get() + e.elems)
        return true;
    }
    return false;
  }
  void SCAPSContext::clearCreated() {
    created.clear();
  }
  SCAPSContext::Entry::Entry() :
    elems(0), data(new PhonemeSpec[ELEMS_PER_BLOCK]) {}
  // ======================== Boilerplate ===========================
  // Let the user stomp over SCA's data as they please... within limits.
  DEF_FUNCS(SCA, SCA_METATABLE_NAME)
  DEF_FUNCS(PhonemeSpec, SCA_PHONEME_METATABLE_NAME)
  DEF_FUNCS(Feature, SCA_FEATURE_METATABLE_NAME)
  DEF_FUNCS(CharClass, SCA_CHARCLASS_METATABLE_NAME)
  // ======================== Utilities =============================
  int messageHandler(lua_State* l) {
    const char* message = lua_tostring(l, 1);
    if (message == nullptr) {
      if (luaL_callmeta(l, 1, "__tostring") &&
          lua_type(l, -1) == LUA_TSTRING) {
        return 1;
      } else {
        message = lua_pushfstring(l, "(error object is a %s value)",
          luaL_typename(l, 1));
      }
    }
    luaL_traceback(l, l, message, 1);
    return 1;
  }
  int mycall(lua_State* l, int nargs, int nres) {
    int base = lua_gettop(l) - nargs;
    lua_pushcfunction(l, messageHandler);
    lua_insert(l, base);
    int status = lua_pcall(l, nargs, nres, base);
    lua_remove(l, base);
    return status;
  }
  SCAPSContext* getSCAPSContext(lua_State* l) {
    lua_getfield(l, LUA_REGISTRYINDEX, ctxName);
    if (!lua_isuserdata(l, -1)) return nullptr;
    auto p = (SCAPSContext*) lua_touserdata(l, -1);
    lua_pop(l, 1);
    return p;
  }
  static std::string_view getStdString(lua_State* l, int arg) {
    size_t len;
    const char* s = luaL_checklstring(l, arg, &len);
    return std::string_view(s, len);
  }
  int createExpression(lua_State* l, const std::string_view& s) {
    char* buffer = new char[s.length() + 7];
    memcpy(buffer, "return ", 7);
    memcpy(buffer + 7, s.data(), s.length());
    int stat = luaL_loadbuffer(l, buffer, s.length() + 7, "<Γ>");
    delete[] buffer;
    if (stat != LUA_OK) return LUA_NOREF;
    return luaL_ref(l, LUA_REGISTRYINDEX);
  }
  // ======================== Functions for Lua =====================
  // ======================== SCA ===================================
  // phoneme = sca:getPhoneme(name)
  int scaGetPhoneme(lua_State* l) {
    SCA* nonconst;
    const SCA* sca = checkForSCAConst(l, 1, &nonconst);
    std::string name = std::string(getStdString(l, 2));
    if (nonconst == nullptr) {
      const PhonemeSpec* ps;
      Error res = sca->getPhonemeByName(name, ps);
      LUA_CHECK_SCA_EC(res)
      pushPhonemeSpecConst(l, *ps);
    } else {
      PhonemeSpec* ps;
      Error res = nonconst->getPhonemeByName(name, ps);
      LUA_CHECK_SCA_EC(res)
      pushPhonemeSpec(l, *ps);
    }
    return 1;
  }
  // index, feature = sca:getFeature(name)
  int scaGetFeature(lua_State* l) {
    SCA* nonconst;
    const SCA* sca = checkForSCAConst(l, 1, &nonconst);
    std::string name = std::string(getStdString(l, 2));
    if (nonconst == nullptr) {
      // const code
      size_t id; const Feature* f;
      Error res = sca->getFeatureByName(name, id, f);
      LUA_CHECK_SCA_EC(res)
      lua_pushinteger(l, id);
      pushFeatureConst(l, *f);
    } else {
      // non-const code
      size_t id; Feature* f;
      Error res = nonconst->getFeatureByName(name, id, f);
      LUA_CHECK_SCA_EC(res)
      lua_pushinteger(l, id);
      pushFeature(l, *f);
    }
    return 2;
  }
  // index, class = sca:getClass(name)
  int scaGetClass(lua_State* l) {
    SCA* nonconst;
    const SCA* sca = checkForSCAConst(l, 1, &nonconst);
    std::string name = std::string(getStdString(l, 2));
    if (nonconst == nullptr) {
      // const code
      size_t id; const CharClass* cc;
      Error res = sca->getClassByName(name, id, cc);
      LUA_CHECK_SCA_EC(res)
      lua_pushinteger(l, id);
      pushCharClassConst(l, *cc);
    } else {
      // non-const code
      size_t id; CharClass* cc;
      Error res = nonconst->getClassByName(name, id, cc);
      LUA_CHECK_SCA_EC(res)
      lua_pushinteger(l, id);
      pushCharClass(l, *cc);
    }
    return 2;
  }
  // feature = sca:getFeatureByIndex(idx)
  int scaGetFeatureByIndex(lua_State* l) {
    SCA* nonconst;
    const SCA* sca = checkForSCAConst(l, 1, &nonconst);
    lua_Integer i = luaL_checkinteger(l, 2);
    luaL_argcheck(l, i >= 0, 1, "Index must be positive");
    if (nonconst == nullptr) {
      const Feature* f = sca->getFeatureByIDOrNull((size_t) i);
      if (f == nullptr) {
        lua_pushnil(l);
        return 1;
      }
      pushFeatureConst(l, *f);
    } else {
      Feature* f = nonconst->getFeatureByIDOrNull((size_t) i);
      if (f == nullptr) {
        lua_pushnil(l);
        return 1;
      }
      pushFeature(l, *f);
    }
    return 1;
  }
  // class = sca:getClassByIndex(idx)
  int scaGetClassByIndex(lua_State* l) {
    SCA* nonconst;
    const SCA* sca = checkForSCAConst(l, 1, &nonconst);
    lua_Integer i = luaL_checkinteger(l, 2);
    luaL_argcheck(l, i >= 0, 1, "Index must be positive");
    if (nonconst == nullptr) {
      const CharClass* cc = sca->getClassByIDOrNull((size_t) i);
      if (cc == nullptr) {
        lua_pushnil(l);
        return 1;
      }
      pushCharClassConst(l, *cc);
    } else {
      CharClass* cc = nonconst->getClassByIDOrNull((size_t) i);
      if (cc == nullptr) {
        lua_pushnil(l);
        return 1;
      }
      pushCharClass(l, *cc);
    }
    return 1;
  }
  // eq = sca:considersEqual(ps1, ps2)
  int scaConsidersEqual(lua_State* l) {
    const SCA* sca = checkForSCAConst(l, 1);
    const PhonemeSpec* ps1 = checkForPhonemeSpecConst(l, 2);
    const PhonemeSpec* ps2 = checkForPhonemeSpecConst(l, 3);
    lua_pushboolean(l, arePhonemeSpecsEqual(*sca, *ps1, *ps2));
    return 1;
  }
  // ======================== PhonemeSpec ===========================
  // name = ps:getName()
  int psGetName(lua_State* l) {
    const PhonemeSpec* ps = checkForPhonemeSpecConst(l, 1);
    lua_pushlstring(l, ps->name.c_str(), ps->name.length());
    return 1;
  }
  // charClass = ps:getCharClass(sca)
  int psGetCharClass(lua_State* l) {
    const PhonemeSpec* ps = checkForPhonemeSpecConst(l, 1);
    SCA* scanc;
    const SCA* sca = checkForSCAConst(l, 2, &scanc);
    if (scanc == nullptr) {
      const CharClass* cc = sca->getClassByIDOrNull(ps->charClass);
      if (cc == nullptr) {
        lua_pushnil(l);
        return 1;
      }
      pushCharClassConst(l, *cc);
    } else {
      CharClass* cc = scanc->getClassByIDOrNull(ps->charClass);
      if (cc == nullptr) {
        lua_pushnil(l);
        return 1;
      }
      pushCharClass(l, *cc);
    }
    return 1;
  }
  // charClassName = ps:getCharClassName(sca)
  int psGetCharClassName(lua_State* l) {
    const PhonemeSpec* ps = checkForPhonemeSpecConst(l, 1);
    const SCA* sca = checkForSCAConst(l, 2);
    const CharClass* cc = sca->getClassByIDOrNull(ps->charClass);
    if (cc == nullptr) {
      lua_pushnil(l);
      return 1;
    }
    lua_pushlstring(l, cc->name.c_str(), cc->name.size());
    return 1;
  }
  // charClassIndex = ps:getCharClassIndex()
  int psGetCharClassIndex(lua_State* l) {
    const PhonemeSpec* ps = checkForPhonemeSpecConst(l, 1);
    lua_pushinteger(l, ps->charClass);
    return 1;
  }
  // fv = ps:getFeatureValue(fid, sca)
  int psGetFeatureValue(lua_State* l) {
    const PhonemeSpec* ps = checkForPhonemeSpecConst(l, 1);
    lua_Integer fid = luaL_checkinteger(l, 2);
    const SCA* sca = checkForSCAConst(l, 3);
    const Feature* f = sca->getFeatureByIDOrNull(fid);
    if (f == nullptr) {
      lua_pushnil(l);
      return 1;
    }
    size_t fv = ps->getFeatureValue(fid, *sca);
    lua_pushinteger(l, fv + 1);
    return 1;
  }
  // fn = ps:getFeatureName(fid, sca)
  int psGetFeatureName(lua_State* l) {
    const PhonemeSpec* ps = checkForPhonemeSpecConst(l, 1);
    lua_Integer fid = luaL_checkinteger(l, 2);
    const SCA* sca = checkForSCAConst(l, 3);
    const Feature* f = sca->getFeatureByIDOrNull(fid);
    if (f == nullptr) {
      lua_pushnil(l);
      return 1;
    }
    size_t fv = ps->getFeatureValue(fid, *sca);
    const std::string& name = f->instanceNames[fv];
    lua_pushlstring(l, name.c_str(), name.size());
    return 1;
  }
  // newps = ps:clone(subs, sca)
  int psClone(lua_State* l) {
    SCAPSContext* ctx = getSCAPSContext(l);
    if (ctx == nullptr || !ctx->canCreate) {
      return luaL_error(l, "cannot clone here");
    }
    const PhonemeSpec* ps = checkForPhonemeSpecConst(l, 1);
    PhonemeSpec newps(*ps);
    luaL_checktype(l, 2, LUA_TTABLE);
    const SCA* sca = checkForSCAConst(l, 3);
    // Iterate through the table
    lua_pushnil(l);
    while (lua_next(l, 2) != 0) {
      // key at -2, value at -1
      size_t fid, iid;
      const Feature* feature;
      if (lua_isnumber(l, -2)) {
        fid = lua_tonumber(l, -2);
        feature = sca->getFeatureByIDOrNull(fid);
        if (feature == nullptr) {
          return luaL_error(l,
            "feature with index %I not found", (lua_Integer) fid);
        }
      } else if (lua_isstring(l, -2)) {
        Error res = sca->getFeatureByName(
          lua_tostring(l, -2), fid, feature);
        if (!res.ok()) {
          return luaL_error(l, "%s", errorAsString(res).c_str());
        }
      } else {
        return luaL_error(l, "key is neither number nor string");
      }
      if (lua_isnumber(l, -1)) {
        iid = lua_tonumber(l, -1) - 1;
      } else if (lua_isstring(l, -1)) {
        Error res = feature->getFeatureInstanceByName(
          lua_tostring(l, -1), iid);
        if (!res.ok()) {
          return luaL_error(l, "%s", errorAsString(res).c_str());
        }
      } else {
        return luaL_error(l, "value is neither number nor string");
      }
      if (iid >= feature->instanceNames.size()) {
        return luaL_error(l,
          "instance with index %I not found", (lua_Integer) iid);
      }
      newps.setFeatureValue(fid, iid, *sca);
      lua_pop(l, 1);
    }
    const PhonemeSpec& res = lookup(*ctx, *sca, newps);
    pushPhonemeSpecConst(l, res);
    return 1;
  }
  // ======================== Feature ===============================
  // name = feature:getName()
  int featureGetName(lua_State* l) {
    const Feature* f = checkForFeatureConst(l, 1);
    lua_pushlstring(l, f->featureName.c_str(), f->featureName.size());
    return 1;
  }
  // instanceNames = feature:getInstanceNames()
  int featureGetInstanceNames(lua_State* l) {
    const Feature* f = checkForFeatureConst(l, 1);
    lua_newtable(l);
    for (size_t i = 0; i < f->instanceNames.size(); ++i) {
      lua_pushlstring(l,
        f->instanceNames[i].c_str(), f->instanceNames[i].size());
      lua_seti(l, -2, i + 1);
    }
    return 1;
  }
  // default = feature:getDefault()
  int featureGetDefault(lua_State* l) {
    const Feature* f = checkForFeatureConst(l, 1);
    lua_pushinteger(l, f->def + 1);
    return 1;
  }
  // isCore = feature:isCore()
  int featureIsCore(lua_State* l) {
    const Feature* f = checkForFeatureConst(l, 1);
    lua_pushboolean(l, f->isCore);
    return 1;
  }
  // isCore = feature:isOrdered()
  int featureIsOrdered(lua_State* l) {
    const Feature* f = checkForFeatureConst(l, 1);
    lua_pushboolean(l, f->ordered);
    return 1;
  }
  // ======================== CharClass =============================
  // name = cc:getName()
  int ccGetName(lua_State* l) {
    const CharClass* cc = checkForCharClassConst(l, 1);
    lua_pushlstring(l, cc->name.c_str(), cc->name.size());
    return 1;
  }
  // ======================== Library info ==========================
  static const luaL_Reg scaMethods[] = {
    {"getPhoneme", scaGetPhoneme},
    {"getFeature", scaGetFeature},
    {"getClass", scaGetClass},
    {"getFeatureByIndex", scaGetFeatureByIndex},
    {"getClassByIndex", scaGetClassByIndex},
    {"considersEqual", scaConsidersEqual},
    {nullptr, nullptr}
  };
  static const luaL_Reg psMethods[] = {
    {"getName", psGetName},
    {"getCharClass", psGetCharClass},
    {"getCharClassName", psGetCharClassName},
    {"getCharClassIndex", psGetCharClassIndex},
    {"getFeatureValue", psGetFeatureValue},
    {"getFeatureName", psGetFeatureName},
    {"clone", psClone},
    {nullptr, nullptr}
  };
  static const luaL_Reg featureMethods[] = {
    {"getName", featureGetName},
    {"getInstanceNames", featureGetInstanceNames},
    {"getDefault", featureGetDefault},
    {"isCore", featureIsCore},
    {"isOrdered", featureIsOrdered},
    {nullptr, nullptr}
  };
  static const luaL_Reg charClassMethods[] = {
    {"getName", ccGetName},
    {nullptr, nullptr}
  };
  static void setLibrary(
      lua_State* l, const char* mtname, const luaL_Reg* tab) {
    luaL_newmetatable(l, mtname);
    lua_pushstring(l, "__index");
    lua_pushvalue(l, -2);
    lua_settable(l, -3);
    luaL_setfuncs(l, tab, 0);
  }
#define SET_LIBRARY(l, mtname, tab) \
  setLibrary(l, mtname, tab); \
  setLibrary(l, MTCONST(mtname), tab);
  static void loadStandardLuaLib(
      lua_State* l, const char* name, lua_CFunction loader) {
    luaL_requiref(l, name, loader, true);
    lua_pop(l, 1);
  }
  // ======================== Call these! ===========================
  // Initialise SCA library
  int init(lua_State* l, InitOptions options) {
    // Standard libraries
    loadStandardLuaLib(l, "_G", luaopen_base);
    loadStandardLuaLib(l, LUA_COLIBNAME, luaopen_coroutine);
    loadStandardLuaLib(l, LUA_STRLIBNAME, luaopen_string);
    loadStandardLuaLib(l, LUA_UTF8LIBNAME, luaopen_utf8);
    loadStandardLuaLib(l, LUA_BITLIBNAME, luaopen_bit32);
    loadStandardLuaLib(l, LUA_MATHLIBNAME, luaopen_math);
    if ((options & InitOptions::io) != InitOptions::none) {
      loadStandardLuaLib(l, LUA_IOLIBNAME, luaopen_io);
      loadStandardLuaLib(l, LUA_LOADLIBNAME, luaopen_package);
    }
    if ((options & InitOptions::os) != InitOptions::none) {
      loadStandardLuaLib(l, LUA_OSLIBNAME, luaopen_os);
    }
    if ((options & InitOptions::debug) != InitOptions::none) {
      loadStandardLuaLib(l, LUA_DBLIBNAME, luaopen_debug);
    }
    // SCA
    SET_LIBRARY(l, SCA_METATABLE_NAME, scaMethods);
    // PhonemeSpec
    SET_LIBRARY(l, SCA_PHONEME_METATABLE_NAME, psMethods);
    // Feature
    SET_LIBRARY(l, SCA_FEATURE_METATABLE_NAME, featureMethods);
    // CharClass
    SET_LIBRARY(l, SCA_CHARCLASS_METATABLE_NAME, charClassMethods);
    // Rule (nothing yet)
    luaL_newmetatable(l, SCA_RULE_METATABLE_NAME);
    // Create metatable for SCAPSContext
    luaL_newmetatable(l, SCA_PSCONTEXT_METATABLE_NAME);
    lua_pushstring(l, "__gc");
    lua_pushcfunction(l, destroySCAPSContext);
    lua_settable(l, -3);
    // Set SCAPSContext
    auto p = (SCAPSContext*) lua_newuserdata(l, sizeof(SCAPSContext));
    new (p) SCAPSContext();
    luaL_getmetatable(l, SCA_PSCONTEXT_METATABLE_NAME);
    lua_setmetatable(l, -2);
    lua_setfield(l, LUA_REGISTRYINDEX, ctxName);
    return 1;
  }
}
