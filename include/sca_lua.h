#pragma once

#include <stddef.h>

#include <memory>
#include <string_view>
#include <vector>

#include <lua.hpp>

namespace sca {
  class SCA;
  struct PhonemeSpec;
  struct Feature;
  struct CharClass;
  namespace lua {
    class SCAPSContext {
    public:
      bool canCreate = false;
      struct Entry {
        Entry();
        size_t elems = 0;
        std::unique_ptr<PhonemeSpec[]> data;
      };
      PhonemeSpec* create(PhonemeSpec&& ps);
      void clearCreated();
      bool contains(const PhonemeSpec* ps) const;
    private:
      void appendElem();
      std::vector<Entry> created;
    };
    // Initialise the Lua bindings for SCA objects
    enum class InitOptions {
      none = 0,
      io = 1,
      os = 2,
      debug = 4,
    };
    inline InitOptions operator&(InitOptions a, InitOptions b) {
      return (InitOptions) ((int) a & (int) b);
    }
    inline InitOptions operator|(InitOptions a, InitOptions b) {
      return (InitOptions) ((int) a | (int) b);
    }
    int init(lua_State* l, InitOptions options = InitOptions::none);
#include "lua/header_macro_magic.h"
    DECL_FUNCS(SCA)
    DECL_FUNCS(PhonemeSpec)
    DECL_FUNCS(Feature)
    DECL_FUNCS(CharClass)
#include "lua/header_cleanup.h"
    SCAPSContext* getSCAPSContext(lua_State* l);
    int createExpression(lua_State* l, const std::string_view& s);
    // Message handler
    int messageHandler(lua_State* l);
    int mycall(lua_State* l, int nargs, int nres);
  }
}
