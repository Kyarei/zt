#define DECL_FUNCS(Type) \
  Type* get##Type##OrNull(lua_State* l, int index = 1); \
  int push##Type(lua_State* l, Type& t); \
  const Type* get##Type##ConstOrNull(lua_State* l, int index = 1, Type** nonconst = nullptr); \
  int push##Type##Const(lua_State* l, const Type& t);
