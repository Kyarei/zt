#pragma once

#define MTCONST(mtname) ("const " mtname)

namespace sca::lua {
  void* mynull = nullptr;
  template<typename T>
  const void* checkConstUDataInternal(
      lua_State* l,
      int arg,
      const char* tname,
      const char* tnamec,
      void*& nc) {
    void* tn = luaL_testudata(l, arg, tname);
    if (tn != nullptr) {
      nc = tn;
      return tn;
    }
    nc = &mynull;
    return luaL_checkudata(l, arg, tnamec);
  }
}

#define CHECK_CONST_UDATA(T, l, arg, tname, nc) \
  sca::lua::checkConstUDataInternal<T>(l, arg, tname, MTCONST(tname), nc)

// TODO: remove [[maybe_unused]] when we're done
#define DEF_CHECK_FUNC(Type, metatableName) \
  [[maybe_unused]] \
  static Type* checkFor##Type(lua_State* l, int index = 1) { \
    void* ud = luaL_checkudata(l, index, metatableName); \
    luaL_argcheck(l, ud != nullptr, index, #Type " expected"); \
    return *(Type**) ud; \
  }
#define DEF_GET_FUNC(Type, metatableName) \
  [[maybe_unused]] \
  Type* get##Type##OrNull(lua_State* l, int index) { \
    void* ud = luaL_checkudata(l, index, metatableName); \
    return *(Type**) ud; \
  }
#define DEF_PUSH_FUNC(Type, metatableName) \
  [[maybe_unused]] \
  int push##Type(lua_State* l, Type& t) { \
    Type** ud = (Type**) lua_newuserdata(l, sizeof(Type*)); \
    luaL_getmetatable(l, metatableName); \
    lua_setmetatable(l, -2); \
    *ud = &t; \
    return 1; \
  }
#define DEF_CHECK_FUNC_CONST(Type, metatableName) \
  [[maybe_unused]] \
  static const Type* checkFor##Type##Const(lua_State* l, int index = 1, Type** nonconst = nullptr) { \
    void* nc; \
    const void* ud = CHECK_CONST_UDATA(Type, l, index, metatableName, nc); \
    if (nonconst != nullptr) *nonconst = *(Type**) nc; \
    luaL_argcheck(l, ud != nullptr, index, #Type " expected"); \
    return *(const Type**) ud; \
  }
#define DEF_GET_FUNC_CONST(Type, metatableName) \
  [[maybe_unused]] \
  const Type* get##Type##ConstOrNull(lua_State* l, int index, Type** nonconst) { \
    void* nc; \
    const void* ud = CHECK_CONST_UDATA(Type, l, index, metatableName, nc); \
    if (nonconst != nullptr) *nonconst = *(Type**) nc; \
    return *(const Type**) ud; \
  }
#define DEF_PUSH_FUNC_CONST(Type, metatableName) \
  [[maybe_unused]] \
  int push##Type##Const(lua_State* l, const Type& t) { \
    const Type** ud = (const Type**) lua_newuserdata(l, sizeof(const Type*)); \
    luaL_getmetatable(l, MTCONST(metatableName)); \
    lua_setmetatable(l, -2); \
    *ud = &t; \
    return 1; \
  }
#define DEF_FUNCS(Type, metatableName) \
  DEF_CHECK_FUNC(Type, metatableName) \
  DEF_GET_FUNC(Type, metatableName) \
  DEF_PUSH_FUNC(Type, metatableName) \
  DEF_CHECK_FUNC_CONST(Type, metatableName) \
  DEF_GET_FUNC_CONST(Type, metatableName) \
  DEF_PUSH_FUNC_CONST(Type, metatableName)

#define LUA_CHECK_SCA_EC(res) \
  if (!(res).ok()) { \
    return luaL_error(l, "%s", errorAsString(res).c_str()); \
  }
