// These have to be macros so the code generation macros won't vomit.
#define SCA_METATABLE_NAME "ztš.SCA"
#define SCA_PHONEME_METATABLE_NAME "ztš.SCA.Phoneme"
#define SCA_FEATURE_METATABLE_NAME "ztš.SCA.Feature"
#define SCA_CHARCLASS_METATABLE_NAME "ztš.SCA.CharClass"
#define SCA_RULE_METATABLE_NAME "ztš.SCA.Rule"
#define SCA_PSCONTEXT_METATABLE_NAME "ztš.SCA.PSContext"
