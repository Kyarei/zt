#pragma once

#include <optional>
#include <string>

#define CHECK_ERROR_IN_VARIANT(x) \
  if (std::holds_alternative<Error>(x)) return std::get<Error>(x);

namespace sca {
  enum class ErrorCode {
    ok,
    noSuchFeature,
    noSuchFeatureInstance,
    featureExists,
    noSuchClass,
    classExists,
    phonemeAlreadyHasClass,
    noSuchPhoneme,
    explicitLabelZero,
    mixedMatchers,
    spacesWrong,
    undefinedMatcher,
    nonCoreFeatureSet,
    enumCharCountMismatch,
    enumToNonEnum,
    invalidOperatorOmega,
    multipleInstancesOmega,
    nonSingleCharInOmega,
    orderedConstraintUnorderedFeature,
    undefinedDependentConstraint,
    lexerGeneric,
    luaSyntax,
    unrecognisedDirective,
    ifdefSpace,
    elseWithoutIf,
    endWithoutIf,
    internalIncludeLua,
    fileError,
    luaSyntaxGamma,
    luaSyntaxOmega,
    matchLimitExceeded,
  };
  struct Error {
    ErrorCode ec;
    std::string details;
    std::optional<std::string> fname;
    size_t line = -1, col = -1;
    Error(ErrorCode ec) : ec(ec) {}
    Error(ErrorCode ec, std::string&& details) :
      ec(ec), details(std::move(details)) {}
    bool ok() const { return ec == ErrorCode::ok; }
    bool operator==(const Error& e) const { return ec == e.ec; }
    bool operator==(const ErrorCode e) const { return ec == e; }
    bool operator!=(const Error& e) const { return ec != e.ec; }
    bool operator!=(const ErrorCode e) const { return ec != e; }
    Error& at(size_t line, size_t col) {
      this->line = line;
      this->col = col;
      return *this;
    }
    Error& at(std::string&& fname, size_t line, size_t col) {
      this->fname = std::move(fname);
      this->line = line;
      this->col = col;
      return *this;
    }
  };
  void printError(const Error& err);
  std::string errorAsString(const Error& err);
  const char* stringError(ErrorCode ec);
  inline Error operator%(ErrorCode ec, const std::string& s) {
    return Error(ec, std::string(s));
  }
  constexpr size_t INVALID = (size_t) -1;
}
