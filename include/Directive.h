#pragma once

#include <stddef.h>

#include <string>
#include <optional>
#include <variant>

namespace sca {
  namespace directive {
    struct If { int luaexpr; bool isElse; };
    struct IfDef { std::string name; bool isElse; };
    struct Else {};
    struct End {};
    struct Define {
      std::string name;
      std::variant<std::string, size_t> value;
    };
    struct Undef {
      std::string name;
    };
    struct Include {
      std::string fname;
      bool isLua;
      bool once;
    };
    struct Export { std::string name; };
  }
  // need to wrap inside struct or Clang + libstdc++ will vomit errors
  struct Directive {
    template<typename T>
    Directive(T&& arg) : value(arg) {}
    template<typename T>
    Directive(const T& arg) : value(arg) {}
    std::variant<
      directive::If,
      directive::IfDef,
      directive::Else,
      directive::End,
      directive::Define,
      directive::Undef,
      directive::Include,
      directive::Export
    > value;
  };
}
