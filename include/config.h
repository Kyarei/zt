#pragma once

#include <stddef.h>

#include <string>
#include <vector>

#include "sca_lua.h"

namespace sca {
  constexpr size_t nWorkThreads = 4;
  extern const char* defaultFormat;
  struct Config {
    const char* script = nullptr;
    const char* words = nullptr;
    const char* format = defaultFormat;
    const char* escapes = "\\";
    std::vector<std::string> pathList;
    std::vector<std::pair<std::string, std::string>> stringValues;
    std::vector<std::pair<std::string, size_t>> numValues;
    size_t nThreads = 0;
    size_t maxSubsts = 5000;
    bool verbose = false;
    bool syntaxOnly = false;
    sca::lua::InitOptions opts = sca::lua::InitOptions::none;
  };
  void parse(Config& c, int argc, char** argv);
}
