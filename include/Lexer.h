#pragma once

#include <stddef.h>

#include <iosfwd>
#include <memory>
#include <optional>
#include <unordered_set>
#include <vector>

#include <boost/filesystem.hpp>

#include <lua.hpp>

#include "Directive.h"
#include "Token.h"
#include "errors.h"

namespace sca {
  class Cursor {
  public:
    Cursor(
        std::shared_ptr<std::istream>&& s,
        std::optional<std::string>&& fname = std::nullopt) :
      s(std::move(s)), fname(std::move(fname)), line(0), col(0), off(0) {}
    int read() noexcept;
    int peek() noexcept;
    std::shared_ptr<std::istream> s;
    std::optional<std::string> fname;
    size_t line, col, off;
  };
  Token empty(const Cursor& c) noexcept;
  class Lexer {
  public:
    Lexer(
      std::shared_ptr<std::istream>&& in,
      const std::optional<std::string>& fname = std::nullopt);
    std::optional<Token> getNext() noexcept;
    size_t getLine() const noexcept { return cursor().line; }
    size_t getCol() const noexcept { return cursor().col; }
    std::optional<std::string> getFName() const noexcept {
      return cursor().fname;
    }
    void addIncludePath(std::string&& s) {
      searchPaths.push_back(std::move(s));
    }
    void setStringVar(const char* key, const std::string& val);
    void setNumVar(const char* key, size_t val);
    void exportVariables(lua_State* l2);
  private:
    Cursor& cursor() {
      return cursors.back();
    }
    const Cursor& cursor() const {
      return cursors.back();
    }
    std::variant<Token, Directive, Error> getNextTD() noexcept;
    void discardWhitespace() noexcept;
    std::string getRestOfLine() noexcept;
    std::string getRestOfLineStrip() noexcept;
    Error handleDirective(const Directive& d) noexcept;
    std::variant<std::string, boost::system::error_code>
    searchFor(const std::string& rpath) const noexcept;
    void addExport(const std::string& name) { exports.push_back(name); }
    std::vector<Cursor> cursors;
    mutable std::unique_ptr<lua_State, decltype(&lua_close)>
      luaState;
    // technically a stack, but we need to look 1 element deep as well
    struct IfEntry {
      bool current;
      bool expired;
      bool isValid() const {
        return current && !expired;
      }
    };
    bool isValid() const {
      return ifs.empty() || ifs.back().isValid();
    }
    std::vector<IfEntry> ifs;
    std::unordered_set<std::string> included;
    std::vector<boost::filesystem::path> searchPaths;
    std::optional<LuaCode> retained;
    std::vector<std::string> exports;
  };
}
